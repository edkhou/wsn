﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WSN.Devices.Sensor
{
    [XmlRoot]
    public class Accel : Sensor
    {
        private List<string> _value = new List<string>() { "0", "0", "0" };
        [JsonProperty("value2")]
        //[XmlElement(ElementName = "Value")]
        //[XmlInclude(List<string>)]
        [XmlArray]
        [XmlArrayItem(ElementName="Value")]
        public List<string> Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                OnPropertyChanged("OutputValue");
            }
        }

        public override string OutputValue
        {
            get
            {
                return string.Format("{0}\n{1}\n{2}", Value[0].Substring(0, Math.Min(Value[0].Length, 4)),
                    Value[1].Substring(0, Math.Min(Value[1].Length, 4)),
                    Value[2].Substring(0, Math.Min(Value[2].Length, 4)));
                /*return string.Format("{0}", Math.Sqrt(
                    Math.Pow(Convert.ToDouble(Value[0]), 2) +
                    Math.Pow(Convert.ToDouble(Value[1]), 2) +
                    Math.Pow(Convert.ToDouble(Value[2]), 2)
                    ));*/
            }
        }

        //like a g-6
        public float ConvertSByteToG(sbyte s)
        {
            int a = Convert.ToInt32(s);
            float b = (float)a / 64;
            //float i = ( Convert.ToInt32(s) / 64 );
            return b;
        }

        public sbyte ConvertIntToSByte(string s)
        {
            try
            {
                String binary = Convert.ToString(Convert.ToInt32(s, 10), 2).PadLeft(8, '0');
                SByte f = Convert.ToSByte(binary, 2);
                return f;
                //SByte a = Convert.ToSByte(i);
                //return a;
            }
            catch (OverflowException ex)
            {
                Logger.Instance.Log("Accel", ex.Message);
            }
            return 0;
        }
    }
}
