﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSN.Sensor
{
    public class Accel : Sensor
    {
        private new IList<string> _value;
        [JsonProperty("value2")]
        public new IList<string> Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                OnPropertyChanged("OutputValue");
            }
        }

        public override string OutputValue
        {
            get
            {
                return string.Format("{0}, {1}, {2}", Value[0], Value[1], Value[2]);
            }
        }
    }
}
