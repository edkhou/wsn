﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSN.Sensor
{
    public class Accel
    {
        [JsonProperty("value")]
        public long Value;

        [JsonProperty("id")]
        public string ID;

        [JsonProperty("value2")]
        public IList<String> AccelValue;
    }
}
