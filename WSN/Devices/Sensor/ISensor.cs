﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSN.Devices.Sensor
{
    public interface ISensor : INotifyPropertyChanged
    {
        int Value { get; set; }
        string ID { get; set; }
        string OutputValue { get; }
        string OutputID { get; }
    }
}
