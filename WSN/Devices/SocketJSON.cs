﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSN.Devices
{
    public class SocketJSON
    {
        /*
         * { name: '' channel: '' light: '' volt: '' temp: '' {accel {x: '' y:'' z:''}}
         */

        [JsonProperty("name")]
        public string Name;

        [JsonProperty("channel")]
        public string Channel;

        [JsonProperty("light")]
        public long Light;

        [JsonProperty("volt")]
        public int Voltage;

        [JsonProperty("temp")]
        public int Temp;

        [JsonProperty("accel")]
        public IList<String> Accel;

        

        //[JsonProperty("X")]
        //public int X;

        //[JsonProperty("Y")]
        //public int Y;

        //[JsonProperty("Z")]
        //public int Z;

    }
}
