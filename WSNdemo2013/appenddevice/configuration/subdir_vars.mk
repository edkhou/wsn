################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../configuration/accel.c \
../configuration/bool.c \
../configuration/button.c \
../configuration/configure.c \
../configuration/corresp.c \
../configuration/i2c.c \
../configuration/io.c \
../configuration/led.c \
../configuration/light.c \
../configuration/lqi.c \
../configuration/pin.c \
../configuration/pwm.c \
../configuration/servo.c \
../configuration/spi.c \
../configuration/temp.c \
../configuration/time.c \
../configuration/voltage.c 

OBJS += \
./configuration/accel.obj \
./configuration/bool.obj \
./configuration/button.obj \
./configuration/configure.obj \
./configuration/corresp.obj \
./configuration/i2c.obj \
./configuration/io.obj \
./configuration/led.obj \
./configuration/light.obj \
./configuration/lqi.obj \
./configuration/pin.obj \
./configuration/pwm.obj \
./configuration/servo.obj \
./configuration/spi.obj \
./configuration/temp.obj \
./configuration/time.obj \
./configuration/voltage.obj 

C_DEPS += \
./configuration/accel.pp \
./configuration/bool.pp \
./configuration/button.pp \
./configuration/configure.pp \
./configuration/corresp.pp \
./configuration/i2c.pp \
./configuration/io.pp \
./configuration/led.pp \
./configuration/light.pp \
./configuration/lqi.pp \
./configuration/pin.pp \
./configuration/pwm.pp \
./configuration/servo.pp \
./configuration/spi.pp \
./configuration/temp.pp \
./configuration/time.pp \
./configuration/voltage.pp 

C_DEPS__QUOTED += \
"configuration\accel.pp" \
"configuration\bool.pp" \
"configuration\button.pp" \
"configuration\configure.pp" \
"configuration\corresp.pp" \
"configuration\i2c.pp" \
"configuration\io.pp" \
"configuration\led.pp" \
"configuration\light.pp" \
"configuration\lqi.pp" \
"configuration\pin.pp" \
"configuration\pwm.pp" \
"configuration\servo.pp" \
"configuration\spi.pp" \
"configuration\temp.pp" \
"configuration\time.pp" \
"configuration\voltage.pp" 

OBJS__QUOTED += \
"configuration\accel.obj" \
"configuration\bool.obj" \
"configuration\button.obj" \
"configuration\configure.obj" \
"configuration\corresp.obj" \
"configuration\i2c.obj" \
"configuration\io.obj" \
"configuration\led.obj" \
"configuration\light.obj" \
"configuration\lqi.obj" \
"configuration\pin.obj" \
"configuration\pwm.obj" \
"configuration\servo.obj" \
"configuration\spi.obj" \
"configuration\temp.obj" \
"configuration\time.obj" \
"configuration\voltage.obj" 

C_SRCS__QUOTED += \
"../configuration/accel.c" \
"../configuration/bool.c" \
"../configuration/button.c" \
"../configuration/configure.c" \
"../configuration/corresp.c" \
"../configuration/i2c.c" \
"../configuration/io.c" \
"../configuration/led.c" \
"../configuration/light.c" \
"../configuration/lqi.c" \
"../configuration/pin.c" \
"../configuration/pwm.c" \
"../configuration/servo.c" \
"../configuration/spi.c" \
"../configuration/temp.c" \
"../configuration/time.c" \
"../configuration/voltage.c" 


