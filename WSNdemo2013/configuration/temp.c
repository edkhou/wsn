#include <string.h>
#include "temp.h"
#include "../Common/printf.h"
#include "../HAL/hal_cc2530znp_target_board.h"

char *getTemp(Temp *temp)
{
	if(temp->inter.enable == false)
		takeValueTemp(temp);
	if(temp->send == false)
		return "";
	char tmp[20]= "";
	strcpy(tmp, ";temp:");
	strcat(tmp, p_int(temp->value));
	return tmp;
}

char *getTempConfig(Temp temp)
{
	char tmp[80]= "";
	strcat(tmp, ";dev:temp;conf:{inf:");
	strcat(tmp, p_int(temp.inter.inf));
	strcat(tmp, ";sup:");
	strcat(tmp, p_int(temp.inter.sup));
	strcat(tmp, ";dif:");
	strcat(tmp, p_int(temp.inter.dif));
	strcat(tmp, ";en:");
	strcat(tmp, p_bool(temp.inter.enable));
	strcat(tmp, ";send:");
	strcat(tmp, p_bool(temp.send));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setTempConfig(Temp *temp, char *conf)
{
	printf("Change Temp Configuration\r\n");
	if (findAttr(conf, "inf"))
		temp->inter.inf = atoi(findAttr(conf, "inf"));
	if (findAttr(conf, "sup"))
		temp->inter.sup = atoi(findAttr(conf, "sup"));
	if (findAttr(conf, "dif"))
		temp->inter.dif = atoi(findAttr(conf, "dif"));
	if (findAttr(conf, "send"))
		if (atoi(findAttr(conf, "send")) == 1)  temp->send = true;
		else temp->send = false;
	if (findAttr(conf, "en"))
		if (atoi(findAttr(conf, "en")) == 1)  temp->inter.enable = true;
		else temp->inter.enable = false;
	return getTempConfig(*temp);
}

bool interrTemp(Temp *temp)
{
	static int lastvaluetemp;
	int inf = temp->inter.inf;
	int sup = temp->inter.sup;
	int dif = temp->inter.dif;

	if(temp->inter.enable)
	{
		int value = takeValueTemp(temp);
		if(inf == 0 && sup == 0 && dif == 0) 	// sending every time : DANGEROUS because it send a lot of message
		{	 	 	 	 	 	 	 	 		// and you can't change after this. need to reboot the sensor
			if (value != lastvaluetemp)
			{
				lastvaluetemp = value;
				return true;
			}
			return false;
		}

		else									// we need to apply a filter
		{
			if( (inf > 0 && value < inf) || (sup > 0 && value > sup) || dif > 0)
			{
				if(dif >0)
				{
					if(abs(lastvaluetemp-value) > dif)
					{
						lastvaluetemp = value;
						return true;
					}
					else
						return false;
				}
				else
				{
					lastvaluetemp = value;
					return true;
				}
			}
			return false;
		}
	}
	return false;
}

unsigned int takeValueTemp(Temp *temp)
{
	temp->value = TempSensor();
	return temp->value;
}

unsigned int TempSensor(void)
{
	ADC10AE0 = 0x00;
	ADC10CTL1 = 0x0000;
	ADC10CTL0 = 0x0000;
	ADC10CTL1 = INCH_10 + ADC10DIV_4;       // Temp Sensor ADC10CLK/4
	ADC10CTL0 = SREF_1 + ADC10SHT_3 + REFON + REF2_5V + ADC10ON + ADC10SR; //ADC10IE
	delayMs(1);
	ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
	// oC = ((A10/1024)*1500mV)-986mV)*1/3.55mV = A10*423/1024 - 278
	while (!(ADC10CTL0 & ADC10IFG));        // Conversion done?
	unsigned long IntDegC = ((ADC10MEM - 673) * 423) / 1024;
	return (IntDegC);
}
