#ifndef CORRESP_H
#define CORRESP_H
#define NB_SENSORS 15

typedef struct Corresp
{
	char name[10];
	unsigned int adresse;
} Corresp;

void initCorresp(Corresp *corresp);
int addCorresp(Corresp *corresp, char *name, unsigned int address);
unsigned int findCorresp(Corresp *corresp, char *name);

#endif
