#ifndef PIN_H
#define PIN_H
#include "bool.h"

#define DIGITAL 0x00
#define ANALOG	0x01
#define I2C		0x02
#define SPI		0x03
#define PORT2   0x02
#define PORT4   0x04

#define OUTPUT	1
#define INPUT 	2
#define PIN3	3	// p2_2
#define PIN5	5	// p2_3
#define PIN7	7	// p2_4
#define PIN4	4	// p4_4
#define PIN6	6	// p4_5
#define PIN8	8	// p4_6

/**
 * functions to configure and see the 6 bits on IO port
 * Io can be:
 * digital input
 * digital output
 * analog input
 * analog output (not all)
 * mode timer B
 * I2C
 * SPI
 * -------------------
 * | /  8  6  4  VCC |
 * | /  7  5  3  GND |
 * -------------------
 */

typedef struct Pin
{
	short name;
	int mode;
	int direction;
	int value;
	bool send;
	// no specified but entered by the init function
	char port;
	char bit;
} Pin;

void initPin(Pin *pin);
char *getPinConfig(Pin pin);
char *setConfigPin(Pin *pin,char *value);
char *getPin(Pin *pin);
void configPin(Pin *pin);
bool getValuePin(Pin *pin);
short gettValuePin(Pin pin);
void setValuePin(Pin pin, bool value);

#endif
