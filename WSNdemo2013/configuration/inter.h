#ifndef INTER_H
#define INTER_H

typedef struct Inter
{
	int inf, sup, dif;
	bool enable;
} Inter;

#endif
