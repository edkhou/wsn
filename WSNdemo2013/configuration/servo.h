/**
 * \file	servo.h
 * \brief	public methods for servo.c
 * \author 	Jan Szymanski
 * \date	September 2012
 */

#ifndef SERVO_H
#define SERVO_H

#include "bool.h"
#include "pin.h"
#include "io.h"

typedef struct Servo
{
	int value1, value2;
	bool servo0, servo1;
	bool send0, send1;
}Servo;

void servoInit(void);
void setServo0(int param);
void setServo1(int param);

#endif
