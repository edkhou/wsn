#ifndef LIGHT_H
#define LIGHT_H
#include "bool.h"
#include "inter.h"

typedef struct Light
{
	int value;
	bool send;
	Inter inter;
} Light;

char *getLight(Light *light);
char *getLightConfig(Light light);
char *setLightConfig(Light *light, char *conf);
bool interrLight(Light *light);
int takeValueLight(Light *light);

#endif
