#include "bool.h"
#include <string.h>
#include "../Common/printf.h"
#include "../Common/utilities.h"

char *p_bool(bool b)
{
	if(b)
		return "1";
	else
		return "0";
}

void printu(char c)
{
	char buff_uart[10];
	buff_uart[0] = c;
	buff_uart[1] = '\0';
	printf(buff_uart);
}

char *p_int(int x)
{
	char buff_uart[10];
	int i = 0, j= 10000;
	while (j >= 1)
	{
		if(x >= j)
		{
			buff_uart[i] = x/j+48;
			i++;
			x = x%j;
		}
		else if( x == 0)
		{
			buff_uart[i] = 48;
			i++;
		}
		j =j /10;
	}
	buff_uart[i] = '\0';
	if(strcmp(buff_uart, "00000") == 0)
		buff_uart[1] = '\0';

	if(strcmp(buff_uart, "") == 0)
	{
		buff_uart[0] = '0';
		buff_uart[1] = '\0';
	}
	return buff_uart;
}

void p_uint(unsigned int x)
{
	char buff_uart[10];
	unsigned int i=0, j=10000;

	while(j >= 1)
	{
		if(x >= j)
		{
			buff_uart[i] = x/j+48;
			i++;
			x = x -(x/j)*j;
		}
		j =j /10;
	}
	if(i==0)
	{
		buff_uart[i] = '0';i++;
	}
	buff_uart[i] = '\0';
	printf(buff_uart);
}

void p_string(char *ch)
{
	printf(ch);
}

char *p_char(char c)
{
	char buff_uart[10];
	buff_uart[0] = c;
	buff_uart[1] = '\0';
	return buff_uart;
}

char *findAttr(char *buffer, char *attr)
{
	char val[50];
	char *i, *j;
	i = strstr(buffer, attr)+strlen(attr)+1;
	if (strstr(buffer, attr) == NULL)
		return NULL;

	if(*i== '{' && strcmp(attr, "name") !=0)
	{
		strncpy(val, i, strchr(i, '}')-i);
		j = strchr(i, '}');	val[strchr(i, '}')-i] = '\0';
	}
	else if (strchr(i, ';') != NULL)
	{
		strncpy(val, i, strchr(i, ';')-i);
		j = strchr(i, ';');	val[strchr(i, ';')-i] = '\0';
	}
	else
	{
		if (strchr(i, ';') != NULL)
		{
			strncpy(val, i, strchr(i, ';')-i);
			j = strchr(i, ';');	val[strchr(i, ';')-i] = '\0';
		}
		else if (strchr(i, '}') != NULL)
		{
			strncpy(val, i, strchr(i, '}')-i);
			j = strchr(i, '}');	val[strchr(i, '}')-i] = '\0';
		}
		else
		{
			strncpy(val, i, strchr(i, '\0')-i);
			j = strchr(i, '\0');	val[strchr(i, '\0')-i] = '\0';
		}
	}
	return val;
}


unsigned char charToHex(char *c)
{
	unsigned char ch = 0x00;
	if( c[1] != '\0')
	{
		if(c[0] >= 48 && c[0] <= 57)		ch =  c[0]-48;
		else if(c[0] >= 65 && c[0] <= 70)	ch =  c[0]-55;

		ch *= 16;
		if(c[1] >= 48 && c[1] <= 57)		ch = ch + c[1]-48;
		else if(c[1] >= 65 && c[1] <= 70)	ch = ch + c[1]-55;
	}
	else
	{
		if(c[0] >= 48 && c[0] <= 57)		ch =  c[0]-48;
		else if(c[0] >= 65 && c[0] <= 70)	ch =  c[0]-55;

	}
	return ch;
}

char *hexToChar(unsigned char c)
{
	char ch[3];
	ch[2]='\0';
	unsigned char t = (c & 0x0F);
	if (t >= 0 && t <= 9)			ch[1] = 48+t;
	else if (t >= 10 && t <= 15)	ch[1] = 55+t;
	t = (c & 0xF0); t = t >> 4;
	if (t >= 0 && t <= 9)			ch[0] = 48+t;
	else if (t >= 10 && t <= 15)	ch[0] = 55+t;
	return ch;
}
