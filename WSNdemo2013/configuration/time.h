#ifndef TIME_H
#define TIME_H
#include "bool.h"

typedef struct Time
{
	int value; // seconds
	bool enable, inter;
} Time;

char *getTimeConfig(Time *time);
void enableTime(Time *time, bool en);
bool enabledTime(Time time);
char *setTime(Time *time, int value);
char *setTimeConfig(Time *time, char *conf);
int getTime(Time time);
bool interrTime(Time *time);

#endif
