#ifndef BUTTON_H
#define BUTTON_H
#include "bool.h"

typedef struct Button
{
	bool send, inter, value;
} Button;

char *getButton(Button button);
char *getButtonConfig(Button button);
char *setButtonConfig(Button *button, char *conf);
bool interrButton(Button *button);
int takeValueButton(Button *button);

#endif
