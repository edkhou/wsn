#include "configure.h"
#include <string.h>
#include <stdlib.h>
#include "../Common/printf.h"
#include "../Common/utilities.h"
#include "../HAL/hal.h"
#include "../ZNP/znp_interface.h"
#include "../ZNP/znp_interface_spi.h"
#include "servo.h"

extern unsigned char tmp[100];
extern unsigned int  state;

void loadConfig(Config *config, int x)
{
	Config conf[1] = {{"18", 18, true,					// name, channel, send						13
					  {45, true, {0,0,10, false}},		// Light:  value, send, inter				2+24
					  {false},							// LQI
					  {45, true, {0,0,0, false}},		// Voltage:value, send, inter				2+24
					  {45, true, {0,0,0, false}},		// Temp:   value, send, inter				2+24
					  //was {0,0,0, false, false, false, 0},	// Accel:  x, y z, senx, seny, senz, mode	5 / mode:0 no mode, 7 motion xyz; 1 motion x;...
					  {0,0,0, true, true, true, 7},
					  {false, false, false, false},		// LED:    led1, led2, send1, send2			2
					  {false, false, false},			// Button: value, send, inter				2
					  {5, true, true},					// Watchdog Timer: value, enable, (inter for check)	2

					  /* was
					  { // IO:
						{{PIN3, ANALOG,  INPUT,  1, true }, // Fire Alarm
						 {PIN4, DIGITAL, OUTPUT, 0, false},
						 {PIN5, I2C, 	 INPUT,  1, true }, // PIR 1 Hanse
						 {PIN6, DIGITAL, OUTPUT, 0, false},
						 {PIN7, I2C,	 INPUT,  1, true }, // PIR 2 Futurlec
						 {PIN8, DIGITAL, OUTPUT, 0, false}},
						  NONE // Mode: NONE, COMPASS, TEMP
					  },
					  */
					  { // IO:
						{{PIN3, ANALOG,  INPUT,  1, false }, // Fire Alarm
						 {PIN4, DIGITAL, OUTPUT, 0, false},
						 {PIN5, I2C, 	 INPUT,  1, false }, // PIR 1 Hanse
						 {PIN6, DIGITAL, OUTPUT, 0, false},
						 {PIN7, I2C,	 INPUT,  1, false }, // PIR 2 Futurlec
						 {PIN8, DIGITAL, OUTPUT, 0, false}},
						  NONE // Mode: NONE, COMPASS, TEMP
					  },

	  	  	  	  	  {90, 90, false, false, false, false},	// SERVO: value1, value2, servo1, servo2, send1, send2			2
					 }};

	config->io.spi = MPL;
	config->io.spiNumPin = 0;
	config->io.spiReg = 0x12;
	config->io.i2c = 0x00;
	config->io.i2cAdd = 0x48;
	config->io.scl  = 5;
	config->io.sda = 3;
	strcpy(config->name, conf[x].name);
	I2C_init();
	config->channel= conf[x].channel;
	config->send= conf[x].send;
	int i;
	for(i=0; i < 6; i++)
	{
		config->io.pin[i].name = conf[x].io.pin[i].name;
		config->io.pin[i].direction = conf[x].io.pin[i].direction;
		config->io.pin[i].mode = conf[x].io.pin[i].mode;
		config->io.pin[i].value = conf[x].io.pin[i].value;
		config->io.pin[i].send = conf[x].io.pin[i].send;
	}
	config->light.value = conf[x].light.value;
	config->light.send = conf[x].light.send;
	config->light.inter.dif = conf[x].light.inter.dif;
	config->light.inter.inf = conf[x].light.inter.inf;
	config->light.inter.sup = conf[x].light.inter.sup;
	config->light.inter.enable = conf[x].light.inter.enable;
	config->accel.x = conf[x].accel.x;
	config->accel.y = conf[x].accel.y;
	config->accel.z = conf[x].accel.z;
	config->accel.sendx = conf[x].accel.sendx;
	config->accel.sendy = conf[x].accel.sendy;
	config->accel.sendz = conf[x].accel.sendz;
	config->accel.mode = conf[x].accel.mode;
	config->led.led1 = conf[x].led.led1;
	config->led.led2 = conf[x].led.led2;
	config->led.send1 = conf[x].led.send1;
	config->led.send2 = conf[x].led.send2;
	config->button.inter = conf[x].button.inter;
	config->button.value = conf[x].button.value;
	config->button.send = conf[x].button.send;
	config->lqi.send = conf[x].lqi.send;
	config->temp.value = conf[x].temp.value;
	config->temp.send = conf[x].temp.send;
	config->temp.inter.dif = conf[x].temp.inter.dif;
	config->temp.inter.inf = conf[x].temp.inter.inf;
	config->temp.inter.sup = conf[x].temp.inter.sup;
	config->temp.inter.enable = conf[x].light.inter.enable;
	config->voltage.value = conf[x].voltage.value;
	config->voltage.send = conf[x].voltage.send;
	config->voltage.inter.dif = conf[x].voltage.inter.dif;
	config->voltage.inter.inf = conf[x].voltage.inter.inf;
	config->voltage.inter.sup = conf[x].voltage.inter.sup;
	config->voltage.inter.enable = conf[x].voltage.inter.enable;
	config->time.inter = conf[x].time.inter;
	config->time.value = conf[x].time.value;
	config->time.enable = conf[x].time.enable;
	config->servo = conf[x].servo;
}

void  dataToJson(Config config, unsigned char* tmp)
{
	unsigned char i, tmplen;
	tmplen = strlen(tmp);
	for (i=0; i<tmplen; i++)
		tmp[i]=' ';
	strcpy(tmp, "{");
	strcat(tmp, getName(config));
	strcat(tmp, getChannel(config));
	strcat(tmp, getLight(&(config.light)));
	strcat(tmp, getLqi(&(config.lqi)));
	strcat(tmp, getVoltage(&(config.voltage)));
	strcat(tmp, getTemp(&(config.temp)));
	strcat(tmp, getButton(config.button));
	strcat(tmp, getAccel(&(config.accel)));
	strcat(tmp, getLed(config.led));
	// was strcat(tmp, getSPI(&(config.io)));
	// was strcat(tmp, getI2C(&(config.io)));
	// was strcat(tmp, getIo(&(config.io)));
	strcat(tmp, "}\r\n");
}

char *getPanId()
{
	char *panId = "-";
	char hexPanId[2] = "--";
	unsigned int dstlen = 2;
	panId[0] = (signed char) getExtendedPanID()[SRSP_DIP_VALUE_FIELD+7];

	static const char alphabet[] = "0123456789ABCDEF";
	unsigned int i = 0;
	while (*panId && 2*i + 1 < dstlen)
	{
		hexPanId[2*i]   = alphabet[*hexPanId / 16];
		hexPanId[2*i+1] = alphabet[*hexPanId % 16];
		++panId;
		++i;
	}
	hexPanId[2*i] = 0;
	return hexPanId;
}

char *getName(Config config)
{
	char tmp[20] = "";
	strcat(tmp, "name:");
	//strcat(tmp, getPanId());
	strcat(tmp, "J2");	// experimental mote
	return tmp;
}

char *getChannel(Config config)
{
	char tmp[20]= "";
	if(config.send)
	{
		strcat(tmp, ";channel:");
		// doesn't workstrcat(tmp, ",CH:");
		strcat(tmp, p_int(config.channel));
	}
	return tmp;
}

void changeChannel(Config *config, int channel)
{
	config->channel = channel;
}

// analyse the command and modify the configuration
int command(Config *config, char *buffer,  unsigned char* tmp)
{
	char command[20], dev[20], value[100];
	char rx_name[20], dev_panid[20];
	volatile int param = 0;
	printf("Command: %s\r\n", buffer);
	strcpy(rx_name, findAttr(buffer, "name"));
	strcpy(dev_panid, getPanId());

	if(strcmp(rx_name, dev_panid) != 0) 	// check name of device
	{
		printf("Current Message is not for this device: %s", buffer);
		strcpy(tmp, "{name:");
		strcat(tmp, config->name);
		strcat(tmp, ";ack:action;status:error}\n");
		return -1;
	}

	strcpy(config->name, rx_name);
	strcpy(command, findAttr(buffer, "command"));			// take the different parameter
	strcpy(dev, findAttr(buffer, "dev"));

	if(!strcmp(command, "action")) 							// modify part of configuration
	{
		strcpy (tmp, "{name:");
		strcat (tmp, rx_name);
		strcpy (value, findAttr(buffer, "value"));
		if     (!strcmp(dev, "accel"))		strcat(tmp, setAccelConfig(&(config->accel), value));
		else if(!strcmp(dev, "button"))		strcat(tmp, setButtonConfig(&(config->button), value));
		else if(!strcmp(dev, "led"))		strcat(tmp, setLedConfig(&(config->led), value));
		else if(!strcmp(dev, "light"))		strcat(tmp, setLightConfig(&(config->light), value));
		else if(!strcmp(dev, "lqi"))		strcat(tmp, setLqiConfig(&(config->lqi), value));
		else if(!strcmp(dev, "temp"))		strcat(tmp, setTempConfig(&(config->temp), value));
		else if(!strcmp(dev, "voltage"))	strcat(tmp, setVoltageConfig(&(config->voltage), value));
		else if(!strcmp(dev, "io"))			strcat(tmp, setIoConfig(&(config->io),value));
		else if(!strcmp(dev, "i2c"))		strcat(tmp, setI2CConfig(&(config->io), value));
		else if(!strcmp(dev, "spi"))		strcat(tmp, setSPIConfig(&(config->io), value));
		else if(!strcmp(dev, "time"))		strcat(tmp, setTimeConfig(&(config->time), value));
		else if(strcmp(dev, "servo0")==0)
		{
			strcpy(value, findAttr(buffer, "value"));
			param = atoi(value+1);
			setServo0(param);
		}
		else if(strcmp(dev, "servo1")==0)
		{
			strcpy(value, findAttr(buffer, "value"));
			param = atoi(value+1);
			setServo1(param);
		}
	}
	else if (!strcmp(command, "request")) 					// request to send state of configuration
	{
		strcpy (tmp, "{name:");
		strcat (tmp, rx_name);
		if     (!strcmp(dev, "accel"))		strcat(tmp, getAccelConfig(config->accel));
		else if(!strcmp(dev, "button"))		strcat(tmp, getButtonConfig(config->button));
		else if(!strcmp(dev, "led"))		strcat(tmp, getLedConfig(config->led));
		else if(!strcmp(dev, "light"))		strcat(tmp, getLightConfig(config->light));
		else if(!strcmp(dev, "lqi"))		strcat(tmp, getLqiConfig(config->lqi));
		else if(!strcmp(dev, "temp"))		strcat(tmp, getTempConfig(config->temp));
		else if(!strcmp(dev, "voltage"))	strcat(tmp, getVoltageConfig(config->voltage));
		else if(!strcmp(dev, "io"))			strcat(tmp, getIoConfig(&(config->io)));
		else if(!strcmp(dev, "i2c"))		strcat(tmp, getI2CConfig(&(config->io)));
		else if(!strcmp(dev, "spi"))		strcat(tmp, getSPIConfig(&(config->io)));
		else if(!strcmp(dev, "time"))		strcat(tmp, getTimeConfig(&(config->time)));
		printf("Request: %s\r\n", tmp);
	}
	else if (!strcmp(command, "reset")) 					// reset module
	{
		printf("Reset & Reboot\r\n");
		state = 3;
		WDTCTL = 0;
	}
	else													// wrong command
	{
		strcpy(tmp, "{name:");
		strcat(tmp, config->name);
		strcat(tmp, ";ack:action;status:nok}\n");
		printf("Wrong Command\r\n");
	}
	return 0;
}

void concatenate_string(char *original, char *add)
{
   while(*original)
      original++;

   while(*add)
   {
      *original = *add;
      add++;
      original++;
   }
   *original = '\0';
}

// placeholder for a config in information memory
//.infoA     : {} > INFOA /* MSP430 INFO FLASH MEMORY SEGMENTS */
//.infoB     : {} > INFOB
//.infoC     : {} > INFOC
//.infoD     : {} > INFOD
//INFOD                 00001000   00000040  00000000  00000040  RWIX
//INFOC                 00001040   00000040  00000000  00000040  RWIX
//INFOB                 00001080   00000040  00000000  00000040  RWIX
//INFOA                 000010c0   00000040  00000000  00000040  RWIX
// 4 x 64 bytes of information memory available
#pragma DATA_SECTION(config1,".infoD")
const unsigned char config1[] = {"config1"};
#pragma DATA_SECTION(config2,".infoC")
const unsigned char config2[] = {"config2"};
#pragma DATA_SECTION(config3,".infoB")
const unsigned char config3[] = {"config3"};
//#pragma DATA_SECTION(config4,".infoA")
//const unsigned char config4[] =
//{"config4"};
//{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
//0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
//0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
//0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xb2, 0x8a, 0x79, 0x8f, 0x81, 0x8e, 0x72, 0x8d, 0xb3, 0x86,
//};
