#ifndef BOOL_H
#define BOOL_H

typedef enum
{
	false, true
} bool;

// functions of printing
void printu(char c);
char *p_int(int x);
char *p_bool(bool b);
void p_uint(unsigned int x);
void p_string(char *ch);
char *p_char(char c);
unsigned char charToHex(char *c);
char *hexToChar(unsigned char c);
char *findAttr(char *buffer, char *attr);

#endif
