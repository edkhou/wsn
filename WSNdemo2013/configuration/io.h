#ifndef IO_H
#define IO_H
#include "pin.h"

/* Functions to configure and view 6 bits on IO port:
 * digital  input/output
 * analogue input/output (not all)
 * mode timer B
 * I2C and SPI
 */

#define NONE 		0x00
#define COMPASS		0x03
#define TEMP		0x04

typedef struct Io
{
	Pin pin[6];
	int mode, i2c, i2cAdd, scl, sda;
	char spi, spiNumPin, spiReg;
} Io;

void initIo(Io *io);
char *getIoConfig(Io *io);
char *setIoConfig(Io *io, char *value);
char *getIo(Io *io);

#endif
