#ifndef VOLTAGE_H
#define VOLTAGE_H
#include "bool.h"
#include "inter.h"

typedef struct Voltage
{
	int value;
	bool send;
	Inter inter;
} Voltage;

char *getVoltage(Voltage *voltage);
char *getVoltageConfig(Voltage voltage);
char *setVoltageConfig(Voltage *voltage, char *conf);
bool interrVoltage(Voltage *voltage);
int takeValueVoltage(Voltage *voltage);

#endif
