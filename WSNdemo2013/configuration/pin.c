#include <string.h>
#include "../Common/printf.h"
#include "pin.h"
#include "../HAL/hal_cc2530znp_target_board.h"

// 2-2 2-3 2-4   4-4 4-5 4-6
// just 4-4 and 2-2 can be analog output
// Function which set the pin in different mode and different value

void initPin(Pin *pin)
{
	//find the port
	if( pin->name == PIN3 || pin->name == PIN5 || pin->name == PIN7 )
		pin->port = 0x02;
	else
		pin->port = 0x04;
	// find the bit
	switch(pin->name)
	{
		case PIN3 : pin->bit = 0x04; break;
		case PIN5 : pin->bit = 0x08; break;
		case PIN7 : pin->bit = 0x10; break;
		case PIN4 : pin->bit = 0x10; break;
		case PIN6 : pin->bit = 0x20; break;
		case PIN8 : pin->bit = 0x40; break;
	}
	configPin(pin);
}

void configPin(Pin *pin)
{
	// Processing
	if(pin->mode ==  DIGITAL)
	{
		if(pin->direction == OUTPUT)
		{
			if(pin->port == PORT2)
			{
				P2SEL &= ~(pin->bit);  	 // 0 GPIO
				P2DIR |= (pin->bit);	 // 1 output
				if(pin->value) 	P2OUT |=  (pin->bit); // 1
				else			P2OUT &= ~(pin->bit); // 0
			}
			else if(pin->port == PORT4)
			{
				P4SEL &= ~(pin->bit);  	 // 0 GPIO
				P4DIR |= (pin->bit);	 // 1 output
				if(pin->value) 	P4OUT |=  (pin->bit); // 1
				else			P4OUT &= ~(pin->bit); // 0
			}
		}
		else if (pin->direction == INPUT)
			if(pin->port == PORT2)
			{
				P2SEL &= ~(pin->bit);	 // 0 GPIO
				P2DIR &= ~(pin->bit);	 // 0 input
				P2REN |=  (pin->bit);	 // 1 pull resistor activated

				if(pin->value == 0 ) 	 // no interrupt
					P2IE  &= ~(pin->bit);// 0 no interrupt
				else
				{
					P2IE  |= (pin->bit);   // 1 interrupt
					P2IES &= ~(pin->bit);  // 1 Low-to-High Edge transition
					//P2IES |=  (pin->bit);// 1 high-to-low transition
				} // P2IFG flag register
			}
			else if(pin->mode == I2C)
			{
				if(pin->port == PORT2)
				{
					P2OUT &= ~pin->bit;
					P2SEL &= ~pin->bit;
				}
				else
				{
					P4OUT &= ~pin->bit;
					P4SEL &= ~pin->bit;
				}
			}
	}
}

char *getPinConfig(Pin pin)
{
	char tmp[80]= "";
	strcat(tmp, ";dev:io;conf:{pin:");
	strcat(tmp, p_int(pin.name));
	strcat(tmp, ";send:");
	strcat(tmp, p_bool(pin.send));
	strcat(tmp, ";mode:");
	strcat(tmp, p_int(pin.mode));
	strcat(tmp, ";dir:");
	strcat(tmp, p_int(pin.direction));
	strcat(tmp, ";val:");
	strcat(tmp, p_int(pin.value));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setConfigPin(Pin *pin,char *value)
{
	printf("Reconfiguration of the pin %d", pin->name);
	if (findAttr(value, "dir") != NULL)
		pin->direction = atoi(findAttr(value, "dir"));
	if (findAttr(value, "mode") != NULL)
		pin->mode = atoi(findAttr(value, "mode"));
	if (findAttr(value, "val") != NULL)
		pin->value = atoi(findAttr(value, "val"));
	if (findAttr(value, "send") != NULL)
		if(strcmp(findAttr(value, "send"),"1") == 0) pin->send = true;
		else										 pin->send = false;
	configPin(pin);
	return getPinConfig(*pin);
}

char *getPin(Pin *pin)
{
	if(pin->send == false)
		return "";
	char tmp[20]= "";
	switch(pin->name)
	{
		case PIN3 : strcat(tmp, ";pin3:");break;
		case PIN4 : strcat(tmp, ";pin4:");break;
		case PIN5 : strcat(tmp, ";pin5:");break;
		case PIN6 : strcat(tmp, ";pin6:");break;
		case PIN7 : strcat(tmp, ";pin7:");break;
		case PIN8 : strcat(tmp, ";pin8:");break;
	}
	if( pin->mode == DIGITAL && pin->direction == OUTPUT)
		strcat(tmp, p_int(pin->value));
	else if ( pin->mode == DIGITAL && pin->direction == INPUT)
		strcat(tmp, p_int(getValuePin(pin)));
	return tmp;
}

int getValuePin(Pin *pin)
{
	if(pin->mode == DIGITAL)
	{
		if(pin->direction == OUTPUT)
		{
			if( pin->port == PORT2)
			{
				if((P2IN & pin->bit))
					pin->value = 1;
			}
			else
				pin->value = 0;
		}
		else if (pin->direction == INPUT)
		{
			if((P2IN & pin->bit))	pin->value = 1;
			else					pin->value = 0;
		}
		return pin->value;
	}
	return 0;
}

void setValuePin(Pin pin, bool value)
{
	if(pin.mode == I2C)
	{
		if(pin.port == 0x02)
		{
			if(value)	P2DIR &=~pin.bit;
			else		P2DIR |= pin.bit;
		}
		else
		{
			if(value)	P4DIR &=~pin.bit;
			else		P4DIR |= pin.bit;
		}
	}
}

short gettValuePin(Pin pin)
{
	printf("\r\n");
	if(pin.mode == DIGITAL)
	{
		if(pin.port == 0x02)
		{
			printf("GET value %d at pin %d", (P2IN & pin.bit), pin.name);
			return  (P2IN & pin.bit);
		}
		else
		{
			printf("GET value %d at pin %d", (P4IN & pin.bit), pin.name);
			return  (P4IN & pin.bit);
		}
	}
	else if (pin.mode == I2C)
	{
		if(pin.port ==  PORT2)
			return (P2IN & pin.bit);
		else
			return (P2IN & pin.bit);
	}
	return 0;
}
