#include <string.h>
#include "../Common/printf.h"
#include "light.h"

char *getLight(Light *light)
{
	if(light->inter.enable == false)
		takeValueLight(light);
	if(light->send == false)
		return "";
	char tmp[20]= "";
	strcat(tmp, ";light:");
	strcat(tmp, p_int(light->value));
	return tmp;
}

char *getLightConfig(Light light)
{
	char tmp[80]= "";
	strcat(tmp, ";dev:light;conf:{inf:");
	strcat(tmp, p_int(light.inter.inf));
	strcat(tmp, ";sup:");
	strcat(tmp, p_int(light.inter.sup));
	strcat(tmp, ";dif:");
	strcat(tmp, p_int(light.inter.dif));
	strcat(tmp, ";en:");
	strcat(tmp, p_bool(light.inter.enable));
	strcat(tmp, ";send:");
	strcat(tmp, p_bool(light.send));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setLightConfig(Light *light, char *conf)
{
	printf("Change Light Configuration\r\n");
	if (findAttr(conf, "inf"))
		light->inter.inf = atoi(findAttr(conf, "inf"));
	if (findAttr(conf, "sup"))
		light->inter.sup = atoi(findAttr(conf, "sup"));
	if (findAttr(conf, "dif"))
		light->inter.dif = atoi(findAttr(conf, "dif"));
	if (findAttr(conf, "send"))
		if (atoi(findAttr(conf, "send")) == 1)  light->send = true;
		else light->send = false;
	if (findAttr(conf, "en"))
		if (atoi(findAttr(conf, "en")) == 1)  light->inter.enable = true;
		else light->inter.enable = false;
	return getLightConfig(*light);
}

bool interrLight(Light *light)
{
	static int lastvaluelight;
	int inf = light->inter.inf;
	int sup = light->inter.sup;
	int dif = light->inter.dif;
	if(light->inter.enable)
	{
		int value = takeValueLight(light);
		if(inf == 0 && sup == 0 && dif == 0) // Sending every time : DANGEROUS because it sends many messages
		{	 	 	 	 	 	 	 	 	 // and you can't change after this. Need to reboot the sensor
			if (value != lastvaluelight)
			{
				lastvaluelight = value;
				return true;
			}
			return false;
		}
		else								 // We need to apply a filter
		{
			if( (inf > 0 && value < inf) || (sup > 0 && value > sup) || dif > 0)
			{
				if(dif >0)
				{
					if(abs(lastvaluelight-value) > dif)
					{
						lastvaluelight = value;
						return true;
					}
					else
						return false;
				}
				else
				{
					lastvaluelight = value;
					return true;
				}
			}
			return false;
		}
	}
	return false;
}

int takeValueLight(Light *light)
{
	light->value = getLightSensor();
	return light->value;
}
