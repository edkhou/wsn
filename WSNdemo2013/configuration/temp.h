#ifndef TEMP_H
#define TEMP_H
#include "bool.h"
#include "inter.h"

typedef struct Temp
{
	unsigned int value;
	bool send;
	Inter inter;
} Temp;

char *getTemp(Temp *temp);
char *getTempConfig(Temp temp);
char *setTempConfig(Temp *temp, char *conf);
bool interrTemp(Temp *temp);
unsigned int takeValueTemp(Temp *temp);
unsigned int TempSensor(void);

#endif
